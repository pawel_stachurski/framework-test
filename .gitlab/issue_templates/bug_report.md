## Summary

(Summarize the bug encountered concisely)\
eg.: Asset Command menu overlaps nodes text in some cases.

## Environment:
eg.:\
Environment: DEV \
OS: Windows 10\
Browser: Chrome 104.0.5112.81

## Expected results:

(What you should see instead)\
eg.: Asset Menu should be displayed correctly.

## Actual results:

(What actually happens)\
eg.: Asset Menu overlaps text.

## Steps to reproduce:

(How one can reproduce the issue - this is very important)\
eg.:
1. Login to plant-graph
2. Search for "150" in search bar and click 1st option.
3. Click on selected node, and then on asset command menu.
4. Observe

(We can also use here "initial conditions", before doing 1st step.)\
eg.:
Initial Conditions: User is logged in and has some nodes relation displayed\
1. Click on selected node, and then on asset command menu.
2. Observe


## Relevant logs and/or screenshots:

(Paste any relevant logs - please use code blocks (```) to format console output, logs, and code, as\
it's very hard to read otherwise.)

## Priority:

(How fast the bug should be fixed. A higher priority means the bug should be placed higher in the backlog and fixed sooner.)

## Possible solution / comments:
