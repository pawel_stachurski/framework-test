import { Component } from '@angular/core';

@Component({
  selector: 'framework-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'jest-test';
}
